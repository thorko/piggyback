# piggyback

- create a deploy token on the private repo in gitlab
- echo "username" > /tmp/user.txt
- echo "password" > /tmp/pass.txt
- kubectl create secret generic gitlabcom --from-file=/tmp/user.txt --from-file=/tmp/pass.txt
- deploy a runner in public repo
